#include "Skybox.h"
#include "gl_core_4_4.h"
#include <stb_image.h>
#include <iostream>
#include "Shader.h"
Skybox::Skybox(std::string const &path)
{
	// Create VAO and setup buffer
	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(m_vertices), &m_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	// Initialise vector of image paths
	faces = {
			path + "_rt.png",
			path + "_lf.png",
			path + "_up.png",
			path + "_dn.png",
			path + "_ft.png",
			path + "_bk.png"
	};

	// Load the cubemap
	loadCubemap();
}
Skybox::~Skybox()
{
}
void Skybox::Draw(Shader * shader)
{
	// Change depth function so depth test passes on equal
	glDepthFunc(GL_LEQUAL);  
	shader->Use();

	// Draw
	glBindVertexArray(m_VAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_cubemapTexture);
	glDrawArrays(GL_TRIANGLES, 0, 36);

	// Unbind
	glBindVertexArray(0);

	// Reset depth function
	glDepthFunc(GL_LESS);
}
void Skybox::loadCubemap()
{
	glGenTextures(1, &m_cubemapTexture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_cubemapTexture);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		// If data is loaded
		if (data)	
		{
			// Set texture of cubemap
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			// Free data after it is used
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}

	// Set texutre draw parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}
