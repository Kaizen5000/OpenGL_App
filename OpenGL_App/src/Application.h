#pragma once
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <iostream>
#include <vector>
struct GLFWwindow;
class Shader;
class Camera;
class Model;
class Skybox;
using std::vector;
class Application {
public:

	Application();
	virtual ~Application();

	// creates a window and begins the game loop which calls update() and draw() repeatedly
	// it first calls startup() and if that succeeds it then starts the loop,
	// ending with shutdown() if m_gameOver is true
	void run(const char* title, int width, int height, bool fullscreen);

	// these functions must be implemented by a derived class
	bool startup();
	void shutdown();
	void update(float deltaTime);
	void draw();

	// wipes the screen clear to begin a frame of drawing
	void clearScreen();

	// sets the colour that the sceen is cleared to
	void setBackgroundColour(float r, float g, float b, float a = 1.0f);

	// show or hide the OS cursor
	void setShowCursor(bool visible);

	// enable or disable v-sync
	void setVSync(bool enabled);

	// sets m_gameOver to true which will close the application safely when the frame ends
	void quit() { m_gameOver = true; }

	// access to the GLFW window
	GLFWwindow* getWindowPtr() const { return m_window; }

	// query if the window has been closed somehow
	bool hasWindowClosed();

	// returns time since application started
	float getTime() const;

	void ProcessInput();
protected:
	virtual bool createWindow(const char* title, int width, int height, bool fullscreen);
	virtual void destroyWindow();
	GLFWwindow*		m_window;

	bool m_gameOver;
	int nanosuitRotation;
	int m_lightPosition;

	const std::string RESOURCE_PATH = "res/";

	Camera * m_camera;
	glm::mat4 m_projection;
	glm::mat4 m_view;

	Skybox * m_skybox;

	Shader * m_nanosuitShader;
	Shader * m_eyeShader;
	Shader * m_femaleShader;
	Shader * m_spiderShader;
	Shader * m_chestShader;
	Shader * m_maleShader;

	Shader * m_lightShader;
	Shader * m_skyboxShader;
	Shader * m_stencilShader;

	Model * m_nanosuit;
	Model * m_eye;
	Model * m_female;
	Model * m_spider;
	Model * m_chest;
	Model * m_male;

	Model * m_stencil;

	vector<Model *> m_models;
	vector<Shader *> m_shaders;
	vector<Model *> m_pointLights;

	glm::vec3 pointLightPositions[4] = {
		glm::vec3(2.3f, -3.3f, -4.0f),	// Moving light
		glm::vec3(-20.f, 7.f, 0.0f),	// Nanosuit light
		glm::vec3(10.0f, -4.f, 0.0f),	// Chest light
		glm::vec3(0.0f,  3.0f, -4.0f)	// Spider light
	};
};