#include "Model.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stb_image.h>
#include "Mesh.h"
#include "Shader.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
///<summary>
/// Constructor
///</summary>
///<param name = "path">The path to the model file</param>
Model::Model(string const & path, bool gamma) : gammaCorrection(gamma)
{
	loadModel(path);
}

Model::~Model()
{
}

///<summary>
/// Calls draw on each mesh
///</summary>
///<param name = "shader">Shader that gets passed into the mesh draw call</param>
void Model::Draw(Shader * shader)
{
	for (unsigned int i = 0; i < meshes.size(); i++)
	{
		meshes[i].Draw(shader);
	}
}

///<summary>
/// Loads the model
///</summary>
///<param name = "path">Model file path</param>
void Model::loadModel(string const & path)
{
	// Create importer object
	Assimp::Importer importer;
	
	const aiScene* scene = importer.ReadFile(path, 
		aiProcess_Triangulate |			// Transform all primitive shapes to triangles
		aiProcess_FlipUVs |				// Flips the texture coordinates on the y-axis where necessary
		aiProcess_GenNormals|			// Generates normals for the model if needed
		aiProcess_CalcTangentSpace);	// Calculates the tangents and bitangents
					

	// Check for errors
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		// Print error
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
		return;
	}

	// Retrieve the directory path of the filepath
	directory = path.substr(0, path.find_last_of('/'));

	// Process ASSIMP's root node recursively
	processNode(scene->mRootNode, scene);
}

///<summary>
/// Recursive function that processes all the nodes, including their children
///</summary>
///<param name = "node">Current node to be processed</param>
///<param name = "scene">The imported data from the file</param>
void Model::processNode(aiNode * node, const aiScene * scene)
{
	// Process each mesh located at the current node
	for (unsigned int i = 0; i < node->mNumMeshes; i++)
	{
		// The node object only contains indices to index the actual objects in the scene. 
		// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).

		// Gets the mesh from the node
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

		// Processes it and adds it to the vector
		meshes.push_back(processMesh(mesh, scene));
	}

	// Recursively check all children
	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}

///<summary>
/// Processes the mesh by extracting the all the different coordinates, indicies, and materials from the assimp mesh object
/// and creating a mesh object that we've defined
///</summary>
///<param name = "mesh">ASSIMP Mesh object that is being processed</param>
///<param name = "scene">The imported data from the file</param>
Mesh Model::processMesh(aiMesh * mesh, const aiScene * scene)
{
	vector<Vertex> vertices;
	vector<unsigned int> indices;
	vector<Texture> textures;

	// Walk through each of the mesh's vertices
	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		// vec3 to hold the coordinates before adding them to the vertex struct
		glm::vec3 vector; 

		// Positions
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.Position = vector;

		// Normals
		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.Normal = vector;

		// Texture coordinates
		if (mesh->mTextureCoords[0]) // Check for texture coordinates
		{
			glm::vec2 vec;
			// a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else
		{
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		}

		// Tangent
		vector.x = mesh->mTangents[i].x;
		vector.y = mesh->mTangents[i].y;
		vector.z = mesh->mTangents[i].z;
		vertex.Tangent = vector;

		// Bitangent
		vector.x = mesh->mBitangents[i].x;
		vector.y = mesh->mBitangents[i].y;
		vector.z = mesh->mBitangents[i].z;
		vertex.Bitangent = vector;
		vertices.push_back(vertex);
	}
	// Iterate through each face (triangle)
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices vector
		for (unsigned int j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		}
	}

	// process materials
	aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

	// we assume a convention for sampler names in the shaders. Each diffuse texture should be named
	// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
	// Same applies to other texture as the following list summarizes:
	// diffuse: texture_diffuseN
	// specular: texture_specularN
	// normal: texture_normalN

	// 1. diffuse maps
	vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
	textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

	// 2. specular maps
	vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
	textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

	// 3. normal maps
	std::vector<Texture> normalMaps = loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
	textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());

	// 4. height maps
	std::vector<Texture> heightMaps = loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
	textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());

	// Return a mesh object created from the extracted mesh data
	return Mesh(vertices, indices, textures);
}

///<summary>
/// Iterates over all the texture locaitons of the given texture type, retrieves the texture's file location and then loads it
///</summary>
///<param name = "mat">The material, contains all textures</param>
///<param name = "type">Texture type enum which describes the type of texture that is being imported</param>
///<param name = "typeName">The name of the this texture type in the shader</param>
vector<Texture> Model::loadMaterialTextures(aiMaterial * mat, aiTextureType type, string typeName)
{
	// Vector of textures to hold the imported textures
	vector<Texture> textures;

	// Iterate through all the textures of the defined type
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
	{
		// Holds the texture file path
		aiString str;
		// Get the texture file path and save it in str
		mat->GetTexture(type, i, &str);

		// check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
		bool skip = false;
		for (unsigned int j = 0; j < textures_loaded.size(); j++)
		{
			// If strcmp returns 0, the file paths for this texture and a texture already loaded are the same
			if (std::strcmp(textures_loaded[j].path.data(), str.C_Str()) == 0)
			{
				// Push the texture that has already been loaded
				textures.push_back(textures_loaded[j]);
				skip = true; // Skip loading
				break;
			}
		}

		// Loads the texture if it isn't already loaded
		if (!skip)
		{
			Texture texture;
			texture.id = TextureFromFile(str.C_Str(), this->directory);
			texture.type = typeName;
			texture.path = str.C_Str();
			textures.push_back(texture);

			// Add this to the vector of textures already loaded
			textures_loaded.push_back(texture);
		}
	}
	return textures;
}
///<summary>
/// Loads the texture from the path + directory
///</summary>
///<param name = "path">The path of the file, it is assumed that it is in the local folder</param>
///<param name = "directory">The directory of the texture</param>
///<param name = "gamma"></param>
unsigned int Model::TextureFromFile(const char * path, const string & directory, bool gamma)
{
	string filename = string(path);
	filename = directory + '/' + filename;

	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}
