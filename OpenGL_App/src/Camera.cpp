#include "Camera.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>

Camera::Camera(GLFWwindow * window): m_window(window)
{
}
Camera::~Camera()
{

}

void Camera::Update(float deltaTime)
{
	if (glfwGetKey(m_window, GLFW_KEY_W) == GLFW_PRESS)
	{
		m_cameraPosition += m_cameraFront * m_cameraMoveSpeed * deltaTime;
	}

	if (glfwGetKey(m_window, GLFW_KEY_S) == GLFW_PRESS)
	{
		m_cameraPosition -= m_cameraFront * m_cameraMoveSpeed * deltaTime;
	}

	if (glfwGetKey(m_window, GLFW_KEY_A) == GLFW_PRESS)
	{
		glm::vec3 dir = glm::normalize(glm::cross(m_cameraFront, m_cameraUp));
		m_cameraPosition -= dir * m_cameraMoveSpeed * deltaTime;
	}

	if (glfwGetKey(m_window, GLFW_KEY_D) == GLFW_PRESS)
	{
		glm::vec3 dir = glm::normalize(glm::cross(m_cameraFront, m_cameraUp));
		m_cameraPosition += dir * m_cameraMoveSpeed * deltaTime;
	}
	ProcessMouse();
}

void Camera::ProcessMouse()
{
	int state = glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_LEFT);
	double xpos, ypos;
	glfwGetCursorPos(m_window, &xpos, &ypos);
	float deltaMouseX = (float)xpos - (float)m_lastMouseXPos;
	float deltaMouseY = (float)ypos - (float)m_lastMouseYPos;
	if (state == GLFW_PRESS)
	{
		m_cameraYaw += deltaMouseX * m_cameraMouseSensitivity;
		m_cameraPitch += -deltaMouseY * m_cameraMouseSensitivity;

		if (m_cameraPitch > 85.0f) m_cameraPitch = 85.0f;
		if (m_cameraPitch < -85.0f) m_cameraPitch = -85.0f;

		CalculateFront();	
	}
	m_viewMatrix = glm::lookAt(m_cameraPosition, m_cameraPosition + m_cameraFront, m_cameraUp);
	// record the current mouse position for use next frame.
	m_lastMouseXPos = xpos;
	m_lastMouseYPos = ypos;
}

void Camera::CalculateFront()
{
	glm::vec3 front;
	front.x = glm::cos(glm::radians(m_cameraYaw)) * glm::cos(glm::radians(m_cameraPitch));
	front.y = glm::sin(glm::radians(m_cameraPitch));
	front.z = glm::sin(glm::radians(m_cameraYaw)) * glm::cos(glm::radians(m_cameraPitch));
	m_cameraFront = glm::normalize(front);
}

void Camera::SetProjection(float fov, float aspect, float a_Near, float a_Far)
{
	m_projectionMatrix = glm::perspective(fov, aspect, a_Near, a_Far);
}

void Camera::SetPosition(const glm::vec3& pos)
{
	m_cameraPosition = pos;
}
void Camera::SetYawPitchRoll(float yaw, float pitch, float roll)
{
	m_cameraYaw = yaw;
	m_cameraPitch = pitch;
	m_cameraRoll = roll;

	CalculateFront();
}

glm::vec3& Camera::GetPosition()
{
	return m_cameraPosition;
}

float Camera::GetYaw()
{
	return m_cameraYaw;
}
float Camera::GetPitch()
{
	return m_cameraPitch;
}
float Camera::GetRoll()
{
	return m_cameraRoll;
}

glm::mat4& Camera::GetView()
{
	return m_viewMatrix;
}
glm::mat4& Camera::GetProjection()
{
	return m_projectionMatrix;
}
glm::mat4 Camera::GetProjectionView()
{
	return m_projectionMatrix * m_viewMatrix;
}

void Camera::Lookat(glm::vec3 target)
{
	glm::vec3 dir = glm::normalize(target - m_cameraPosition);
	m_cameraPitch = glm::degrees(glm::asin(dir.y));
	m_cameraYaw = glm::degrees(atan2(dir.y, dir.x));

	CalculateFront();
}
