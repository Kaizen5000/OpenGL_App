#include "Application.h"
#include "gl_core_4_4.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <iostream>
#include "Shader.h"
#include <vector>
#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Camera.h"
#include "Model.h"
#include "Skybox.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;
Application::Application(): m_window(nullptr), m_gameOver(false)
{
	nanosuitRotation = 0;
	m_lightPosition = 0;
}
Application::~Application() 
{
	delete m_skyboxShader;
	delete m_skybox;
	for (auto iter = m_pointLights.begin(); iter != m_pointLights.end(); ++iter)
	{
		delete(*iter);
	}
	for (auto iter = m_models.begin(); iter != m_models.end(); ++iter)
	{
		delete(*iter);
	}
	for (auto iter = m_shaders.begin(); iter != m_shaders.end(); ++iter)
	{
		delete(*iter);
	}
	delete m_lightShader;
	delete m_camera;
}
bool Application::createWindow(const char* title, int width, int height, bool fullscreen) 
{
	// Initialises the GLFW library
	if (glfwInit() == GL_FALSE) return false;

	// Creates window from the input parameters
	m_window = glfwCreateWindow(width, height, title, (fullscreen ? glfwGetPrimaryMonitor() : nullptr), nullptr);

	// Checks if the window was not successfully created
	if (m_window == nullptr) 
	{
		// Terminate the GLFW library
		glfwTerminate();
		return false;
	}

	// Makes the context of the window current on this thread
	glfwMakeContextCurrent(m_window);

	// Initialising OpenGL Loader Generator
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED) 
	{
		glfwDestroyWindow(m_window);
		glfwTerminate();
		return false;
	}

	// Resizes the viewport every time the window is resized
	// The second parameter is a function, a lambda is used to change the viewport using the new height and width
	glfwSetWindowSizeCallback(m_window, [](GLFWwindow*, int w, int h) { glViewport(0, 0, w, h); });

	// Specify the clear colour for use in the colour buffers
	glClearColor(1.f, 1.0f, 1.0f, 1.0f);	// R, G, B, Alpha

	// Enable depth
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	// Cull
	glEnable(GL_CULL_FACE);
	
	// Stencil test
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	return true;
}
void Application::destroyWindow() 
{
	glfwDestroyWindow(m_window);
	glfwTerminate();
}
void Application::run(const char* title, int width, int height, bool fullscreen) 
{
	// start game loop if successfully initialised
	if (createWindow(title, width, height, fullscreen) && startup()) 
	{
		// variables for timing
		double prevTime = glfwGetTime();
		double currTime = 0;
		double deltaTime = 0;

		m_view = glm::lookAt(vec3(10, 10, 10), vec3(0), vec3(0, 1, 0));
		m_projection = glm::perspective(glm::pi<float>() * 0.25f, 16 / 9.f, 0.1f, 1000.f);

		// loop while game is running
		while (!m_gameOver) {

			// Update delta time
			currTime = glfwGetTime();
			deltaTime = currTime - prevTime;
			prevTime = currTime;

			// Update window events (input etc)
			glfwPollEvents();

			// Skip if minimised
			if (glfwGetWindowAttrib(m_window, GLFW_ICONIFIED) != 0)	continue;

			// Update
			update(float(deltaTime));

			// Draw
			draw();

			// If either is true, assign to m_gameOver so that the loop exits to end application
			m_gameOver = m_gameOver || glfwWindowShouldClose(m_window) == GLFW_TRUE;
		}
	}

	// cleanup
	shutdown();
	destroyWindow();
}
bool Application::startup()
{
	// Setup camera starting positions
	m_camera = new Camera(m_window);
	m_camera->SetProjection(glm::radians(45.0f), 1280.f / 720.f, 0.1f, 1000.0f);
	m_camera->SetYawPitchRoll(270.f, 0.f, 0.f);
	m_camera->SetPosition(vec3(0.f, 0.f, 20.f));

	// Create shaders for the models, this is setup so that the shader can be swapped out for an individual model in the 
	// future if need be
	m_nanosuitShader	= new Shader((RESOURCE_PATH + "Shaders/MultiLit.vert").c_str(), (RESOURCE_PATH + "Shaders/MultiLit.frag").c_str());
	m_eyeShader			= new Shader((RESOURCE_PATH + "Shaders/MultiLit.vert").c_str(), (RESOURCE_PATH + "Shaders/MultiLit.frag").c_str());
	m_femaleShader		= new Shader((RESOURCE_PATH + "Shaders/MultiLit.vert").c_str(), (RESOURCE_PATH + "Shaders/MultiLit.frag").c_str());
	m_spiderShader		= new Shader((RESOURCE_PATH + "Shaders/MultiLit.vert").c_str(), (RESOURCE_PATH + "Shaders/MultiLit.frag").c_str());
	m_chestShader		= new Shader((RESOURCE_PATH + "Shaders/MultiLit.vert").c_str(), (RESOURCE_PATH + "Shaders/MultiLit.frag").c_str());
	m_maleShader		= new Shader((RESOURCE_PATH + "Shaders/MultiLit.vert").c_str(), (RESOURCE_PATH + "Shaders/MultiLit.frag").c_str());

	// Shader for the point light
	m_lightShader = new Shader((RESOURCE_PATH + "Shaders/Lamp.vert").c_str(),(RESOURCE_PATH + "Shaders/Lamp.frag").c_str());
	// Shader for the point light outline
	m_stencilShader = new Shader((RESOURCE_PATH + "Shaders/Stencil.vert").c_str(), (RESOURCE_PATH + "Shaders/Stencil.frag").c_str());

	// Add all shaders to the shader vector
	m_shaders.push_back(m_nanosuitShader);
	m_shaders.push_back(m_eyeShader);
	m_shaders.push_back(m_femaleShader);
	m_shaders.push_back(m_spiderShader);
	m_shaders.push_back(m_chestShader);
	m_shaders.push_back(m_maleShader);

	// Create models
	m_nanosuit	= new Model((RESOURCE_PATH + "Objects/nanosuit/nanosuit.obj").c_str());
	m_eye		= new Model((RESOURCE_PATH + "Objects/HumanEye/Human_Eye.obj").c_str());
	m_female	= new Model((RESOURCE_PATH + "Objects/Ivy/R_ALL_00b_360.obj").c_str());
	m_spider	= new Model((RESOURCE_PATH + "Objects/OBJ/spider.obj").c_str());
	m_chest		= new Model((RESOURCE_PATH + "Objects/Chest/Chest.obj").c_str());
	m_male		= new Model((RESOURCE_PATH + "Objects/Apprentice/TheApprentice.obj").c_str());

	// Add all models to the vector
	m_models.push_back(m_nanosuit);
	m_models.push_back(m_eye);
	m_models.push_back(m_female);
	m_models.push_back(m_spider);
	m_models.push_back(m_chest);
	m_models.push_back(m_male);

	// Load point light model
	Model * model;
	for (int i = 0; i < 4; ++i)
	{
		model = new Model((RESOURCE_PATH + "Objects/HumanEye/Human_Eye.obj").c_str());
		m_pointLights.push_back(model);
	}

	// Instance of skybox and a shader for the skybox
	m_skybox		= new Skybox((RESOURCE_PATH + "Textures/sky/sky"));
	m_skyboxShader	= new Shader((RESOURCE_PATH + "Shaders/Skybox.vert").c_str(), (RESOURCE_PATH + "Shaders/Skybox.frag").c_str());
	return true;
}

void Application::shutdown()
{
}

void Application::update(float deltaTime)
{
	// Update camera
	m_camera->Update(deltaTime);

	// Process any inputs
	ProcessInput();

	// Clear the screen
	clearScreen();

	// Used to set the model uniform
	mat4 model;
	
	// Set up uniforms that the models have in common
	for (unsigned int i = 0; i < m_shaders.size(); ++i)
	{
		m_shaders[i]->Use();
		m_shaders[i]->setMat4("view", m_camera->GetView());
		m_shaders[i]->setMat4("projection", m_camera->GetProjection());
		m_shaders[i]->setVec3("objectColor", 1.0f, 0.5f, 0.31f);
		m_shaders[i]->setVec3("lightColor", 1.0f, 1.0f, 1.0f);
		m_shaders[i]->setVec3("lightPos", m_camera->GetPosition());
	}

	// Update the rotation value for the nanosuit
	nanosuitRotation += 1;
	nanosuitRotation %= 360;

	// Translate and scale so they're similar size and are lined up
	m_eyeShader->Use();
	model = mat4(1.f);
	model = glm::translate(model, glm::vec3(-40.f, 0.f, 0.f));
	model = glm::scale(model, glm::vec3(10.f, 10.f, 10.f));
	m_eyeShader->setMat4("model", model);

	m_nanosuitShader->Use();
	model = mat4(1.f);
	model = glm::translate(model, glm::vec3(-20.f, 0.f, 0.0f));	
	// Rotate nanosuit
	model = glm::rotate(model, glm::radians((float)nanosuitRotation), vec3(1.f, 0.f, 0.f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	m_nanosuitShader->setMat4("model", model);

	m_femaleShader->Use();
	model = mat4(1.f);
	model = glm::translate(model, glm::vec3(-10.f, 0.f, 0.f));
	model = glm::scale(model, glm::vec3(3.f, 3.f, 3.f));
	m_femaleShader->setMat4("model", model);

	m_spiderShader->Use();
	model = mat4(1.f);
	model = glm::translate(model, glm::vec3(0.f, 0.f, 0.f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	m_spiderShader->setMat4("model", model);

	m_chestShader->Use();
	model = mat4(1.f);
	model = glm::translate(model, glm::vec3(10.0f, 0.f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	m_chestShader->setMat4("model", model);

	m_maleShader->Use();
	model = mat4(1.f);
	model = glm::translate(model, glm::vec3(20.f, 0.f, 0.f));
	model = glm::scale(model, glm::vec3(3.f, 3.f, 3.f));
	m_maleShader->setMat4("model", model);
	
	m_stencilShader->Use();
	model = mat4(1.f);
	m_stencilShader->setMat4("model", model);

	// For each model, set the uniforms for the lighting
	for (unsigned int i = 0; i < m_models.size(); ++i)
	{
		m_shaders[i]->Use();
		m_shaders[i]->setVec3("viewPos", m_camera->GetPosition());
		m_shaders[i]->setFloat("material.shininess", 32.0f);

		// Directional light
		m_shaders[i]->setVec3("dirLight.direction", -0.2f, -1.0f, -0.3f);
		m_shaders[i]->setVec3("dirLight.ambient", 0.05f, 0.05f, 0.05f);
		m_shaders[i]->setVec3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
		m_shaders[i]->setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);

		// SpotLight
		m_shaders[i]->setVec3("spotLight.position", m_camera->GetPosition());
		m_shaders[i]->setVec3("spotLight.direction", m_camera->getHeading());
		m_shaders[i]->setVec3("spotLight.ambient", 0.0f, 0.0f, 0.0f);
		m_shaders[i]->setVec3("spotLight.diffuse", 1.0f, 1.0f, 1.0f);
		m_shaders[i]->setVec3("spotLight.specular", 1.0f, 1.0f, 1.0f);
		m_shaders[i]->setFloat("spotLight.constant", 1.0f);
		m_shaders[i]->setFloat("spotLight.linear", 0.09f);
		m_shaders[i]->setFloat("spotLight.quadratic", 0.032f);
		m_shaders[i]->setFloat("spotLight.cutOff", glm::cos(glm::radians(12.5f)));
		m_shaders[i]->setFloat("spotLight.outerCutOff", glm::cos(glm::radians(15.0f)));

		// Set the uniforms for each point light in the scene
		for (unsigned int i = 0; i < m_pointLights.size(); ++i)
		{
			std::string str = "pointLights[" + std::to_string(i) + "]";
			m_shaders[i]->setVec3((str + ".position").c_str(), pointLightPositions[i]);
			m_shaders[i]->setVec3((str + ".ambient").c_str(), 0.05f, 0.05f, 0.05f);
			m_shaders[i]->setVec3((str + ".diffuse").c_str(), 0.8f, 0.8f, 0.8f);
			m_shaders[i]->setVec3((str + ".specular").c_str(), 1.0f, 1.0f, 1.0f);
			m_shaders[i]->setFloat((str + ".constant").c_str(), 1.0f);
			m_shaders[i]->setFloat((str + ".linear").c_str(), 0.022f);
			m_shaders[i]->setFloat((str + ".quadratic").c_str(), 0.0019f);
		}
	}

	// Skybox
	m_skyboxShader->Use();
	// Remove translation from the view matrix
	mat4 view = mat4(mat3(m_camera->GetView())); 
	m_skyboxShader->setMat4("view", view);
	m_skyboxShader->setMat4("projection", m_camera->GetProjection());

	// Point lights
	m_lightShader->Use();
	m_lightShader->setMat4("view", m_camera->GetView());
	m_lightShader->setMat4("projection", m_camera->GetProjection());

	// Point light stencil
	m_stencilShader->Use();
	m_stencilShader->setMat4("view", m_camera->GetView());
	m_stencilShader->setMat4("projection", m_camera->GetProjection());

	// Point light positions
	m_lightPosition += 1;
	m_lightPosition = (m_lightPosition > 50) ? -50:m_lightPosition;
	pointLightPositions[0].x = (float)m_lightPosition;
}

void Application::draw()
{
	// Ignore from stencil buffer
	glStencilMask(0x00);
	m_skybox->Draw(m_skyboxShader);

	// 1st pass draw, draw to stencil buffer
	glStencilFunc(GL_ALWAYS, 1, 0xFF);
	glStencilMask(0xFF);
	
	// Draw models
	for (unsigned int i = 0; i < m_models.size(); ++i)
	{
		m_models[i]->Draw(m_shaders[i]);
	}
	mat4 model;

	// Draw point lights
	m_lightShader->Use();
	for (unsigned int i = 0; i < m_pointLights.size(); ++i)
	{
		model = mat4(1.f);
		model = glm::translate(model, pointLightPositions[i]);
		model = glm::scale(model, glm::vec3(5.f, 5.f, 5.f));
		m_lightShader->setMat4("model", model);
		m_pointLights[i]->Draw(m_lightShader);
	}

	// 2nd pass draw, draw slightly scaled point lights to the stencil buffer
	glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
	glStencilMask(0x00);
	glDisable(GL_DEPTH_TEST);
	m_stencilShader->Use();
	for (unsigned int i = 0; i < m_pointLights.size(); ++i)
	{
		model = mat4(1.f);
		vec3 pos = vec3(pointLightPositions[i].x, pointLightPositions[i].y - 0.3, pointLightPositions[i].z);
		model = glm::translate(model, pos);

		model = glm::scale(model, glm::vec3(6.f, 6.f, 6.f));
		m_stencilShader->setMat4("model", model);
		m_pointLights[i]->Draw(m_stencilShader);
	}

	// Set to not draw to stencil and enable depth test again
	glStencilMask(0xFF);
	glEnable(GL_DEPTH_TEST);

	// Swap to the buffer which has finished being drawn
	glfwSwapBuffers(m_window);
}

bool Application::hasWindowClosed() 
{
	return glfwWindowShouldClose(m_window) == GL_TRUE;
}

void Application::clearScreen() 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Application::setBackgroundColour(float r, float g, float b, float a) 
{
	glClearColor(r, g, b, a);
}

void Application::setVSync(bool enable) 
{
	glfwSwapInterval(enable ? 1 : 0);
}

void Application::setShowCursor(bool visible) 
{
	ShowCursor(visible);
}

float Application::getTime() const 
{
	return (float)glfwGetTime();
}

void Application::ProcessInput()
{
	// If ESC is pressed, close window
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(m_window, true);
	}
}
