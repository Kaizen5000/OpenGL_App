#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <iostream>
class Shader;
using std::string;
using std::vector;
using glm::vec2;
using glm::vec3;

// Vertex Data
struct Vertex {
	vec3 Position;
	vec3 Normal;
	vec2 TexCoords;
	vec3 Tangent;
	vec3 Bitangent;
};

// Texture Data
struct Texture {
	unsigned int id;
	string type;
	string path;
};

class Mesh
{
public:
	vector<Vertex> m_vertices;
	vector<unsigned int> m_indices;
	vector<Texture> m_textures;
	unsigned int m_VAO;
	Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures);
	~Mesh();
	void Draw(Shader * shader);
private:
	unsigned int m_VBO, m_EBO;
	void SetupMesh();
};

