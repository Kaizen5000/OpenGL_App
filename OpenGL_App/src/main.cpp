#include "Application.h"
int main()
{
	auto app = new Application();
	app->run("OpenGL App", 1280, 720, false);
	delete app;
    return 0;
}

