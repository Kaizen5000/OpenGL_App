#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include "gl_core_4_4.h"
#include <GLFW/glfw3.h>

class Camera
{
public:

	Camera(GLFWwindow * window);
	virtual ~Camera();

	virtual void Update(float deltaTime);

	void SetPosition(const glm::vec3 &pos);
	void SetYawPitchRoll(float yaw, float pitch, float roll);
	void SetProjection(float fov, float aspect, float near, float far);

	glm::vec3& GetPosition();

	float GetYaw();
	float GetPitch();
	float GetRoll();

	glm::mat4& GetView();
	glm::mat4& GetProjection();
	glm::mat4 GetProjectionView();
	const glm::vec3& getHeading() const { return m_cameraFront; }
	void Lookat(glm::vec3 target);

	

protected:
	void ProcessMouse();
	void CalculateFront();

protected:

	glm::vec3 m_cameraPosition = glm::vec3(0, 2, -2);
	glm::vec3 m_cameraFront = glm::vec3(0, 0, -1);
	glm::vec3 m_cameraUp = glm::vec3(0, 1, 0);

	float m_cameraYaw = 0.0f;
	float m_cameraPitch = 0.0f;
	float m_cameraRoll = 0.0f;

	float m_cameraMoveSpeed = 10.0f;
	float m_cameraMouseSensitivity = 0.6f;

	double m_lastMouseXPos, m_lastMouseYPos;

	// camera transforms
	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;

	GLFWwindow*		m_window;
	bool mousedown = false;
private:
};
